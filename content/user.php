<!doctype html>
<html lang="en">
    <!-- include connection -->
<?php require "../include/config.php"; ?>
    <!-- include head code here -->
  <?php include "../include/head.php"; ?>

<body>
	<!-- WRAPPER -->
	<div id="wrapper">
		<!-- NAVBAR -->
		<?php include "../include/nav.php"; ?>
		<!-- END NAVBAR -->
		<!-- LEFT SIDEBAR -->
		<?php include "../include/sidebar.php"; ?>
		<!-- END LEFT SIDEBAR -->
		<!-- MAIN -->
		<div class="main">
			<!-- MAIN CONTENT -->
			<div class="main-content">
				<div class="container-fluid">
					<!-- END OVERVIEW -->
					<div class="row">
						<div class="col-md-12">
							<!-- RECENT PURCHASES -->
							<div class="panel">
								<div class="panel-heading">
									<h3 class="panel-title">Users</h3>
									<div class="right">
										<button type="button" class="btn-toggle-collapse"><i class="lnr lnr-chevron-up"></i></button>
										<button type="button" class="btn-remove"><i class="lnr lnr-cross"></i></button>
									</div>
								</div>
								<div class="panel-body no-padding">
                                    <div class="panel-body">
                                        <table id="bootstrap-data-table-export" class="table table-striped table-bordered">
                                            <thead>
                                                <tr>
                                                    <th> # </th>
                                                    <th> Employee Name </th>
                                                    <th> Email </th>
                                                    <th>User Level</th>
                                                    <th> Created By </th>
                                                    <th> Created Date </th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                                    $sql=mysqli_query($conn,"SELECT * FROM user");
                                                    
                                                    $numRows = mysqli_num_rows($sql); 
                                                
                                                    if($numRows > 0) {
                                                        $i = 1;
                                                        while($row = mysqli_fetch_assoc($sql)) {

                                                        $employeeName = $row['employeeName'];
                                                        $email  = $row['email'];
                                                        $userLevel   = $row['userLevel'];
                                                        $createdBy   = $row['createdBy'];
                                                        $cdate   = $row['cdate'];
                                                        echo ' <tr>';
                                                        echo ' <td>'.$i.' </td>';
                                                        echo ' <td>'.$employeeName.' </td>';
                                                        echo ' <td>'.$email.' </td>';
                                                        echo ' <td>'.$userLevel.' </td>';
                                                        echo ' <td>'.$createdBy.' </td>';
                                                        echo ' <td>'.$cdate.' </td>';
                                                        echo ' </tr>';
                                                        $i++;
                                                        }
                                                    }
                                                ?>
                                            </tbody>
                                        </table>
                                    </div>
								</div>
								<div class="panel-footer">
									<div class="row">
										<div class="col-md-6"><span class="panel-note"><i class="fa fa-clock-o"></i> Last 24 hours</span></div>
										<div class="col-md-6 text-right"><a href="#" class="btn btn-primary">View All Purchases</a></div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="clearfix"></div>
		<?php include "../include/footer.php"; ?>
	</div>
	<!-- END WRAPPER -->
	<!-- Javascript -->
	<?php include "../include/footer-js.php"; ?>
</body>

</html>
