<?php
// Database Connection
require "../include/config.php";
// inspections
if (isset($_POST["form"])) {
  $form = $_POST["form"];
  if ($form === "postJob") {
    // job
    $spare_parts = $_POST["spare_parts"];
    $jobArray = json_decode($_POST["jobdata"], true);
    $technicalDescription = $jobArray[0]["technicalDescription"];
    $jobType = $jobArray[0]["jobType"];
    $jstatus = $jobArray[0]["jstatus"];
    $jobId = $jobArray[0]["jobId"];
    $deliverTime = $jobArray[0]["deliverTime"];
    $jobAssign = $jobArray[0]["jobAssign"];
    $proof = $jobArray[0]["proof"];
    $reason = $jobArray[0]["reason"];

    $updateJob = "UPDATE job SET technicalDescription='$technicalDescription',jstatus='$jstatus', deliverTime='$deliverTime',jobAssign='$jobAssign',proof='$proof', reason='$reason' WHERE  jobId='$jobId'";
    $resultJob = mysqli_query($conn, $updateJob);
    if ($resultJob) {
      echo 1;
    } else {
      echo mysqli_error($conn);
    }
    // insert spareparts
    if (sizeof($spare_parts) > 0) {
      $queryCheck =
        "SELECT job_id from job_sparepart WHERE job_id ='" . $jobId . "'";
      // check for an entry in the table
      $poInserted = false;
      if ($result = mysqli_query($conn, $queryCheck)) {
        if (mysqli_num_rows($result) == 0) {
          // no rows found ;
          foreach ($spare_parts as $part) {
            $sparepart = $part["spId"];
            $orderedQuantity = $part["orderedQuantity"];
            $insertSpare = "INSERT INTO job_sparepart (job_id, sparepartid, orderedQuantity) VALUES ('$jobId','$sparepart','$orderedQuantity')";
            $resultSpare = mysqli_query($conn, $insertSpare);
            if ($part["spQuantity"] < $part["orderedQuantity"]) {
              date_default_timezone_set("Asia/Calcutta");
              $poTime = date("H:i:");
              $poDate = date("Y/m/d");
              if (!$poInserted) {
                $insertPo = "INSERT INTO purchase_order (job_id,poTime,poDate) VALUES ('$jobId','$poTime','$poDate')";
                $resultPo = mysqli_query($conn, $insertPo);
                if ($resultPo) {
                  $poInserted = true;
                  echo 1;
                } else {
                  echo mysqli_error($conn);
                }
              }
              $qrySelectPOid =
                "SELECT * FROM purchase_order WHERE job_id='" . $jobId . "'";
              $sqlSelectPOid = mysqli_query($conn, $qrySelectPOid);
              $row = mysqli_fetch_assoc($sqlSelectPOid);
              $po_id = $row["po_id"];
              $insertPoSpare = "INSERT INTO po_sparepart (po_id,spid) VALUES ('$po_id','$sparepart')";
              $resultPoSpare = mysqli_query($conn, $insertPoSpare);
              if ($resultPoSpare) {
                echo 1;
              } else {
                echo mysqli_error($conn);
              }
            }
          }
        } else {
          // row exist in job_sparepart
          foreach ($spare_parts as $part) {
            $sparepart = $part["spId"];
            $orderedQuantity = $part["orderedQuantity"];
            //check for an entry in the table for that spare part in job_sparepart
            $queryCheck1 =
              "SELECT job_id from job_sparepart WHERE job_id ='" .
              $jobId .
              "' AND sparepartid='" .
              $sparepart .
              "'";
            if ($result1 = mysqli_query($conn, $queryCheck1)) {
              if (mysqli_num_rows($result1) == 0) {
                // no rows
                $insertSpare = "INSERT INTO job_sparepart (job_id, sparepartid, orderedQuantity) VALUES ('$jobId','$sparepart','$orderedQuantity')";
                $resultSpare = mysqli_query($conn, $insertSpare);
                if ($resultSpare) {
                  echo 1;
                } else {
                  echo mysqli_error($conn);
                }
              } else {
                // row exist in job_sparepart
                $updateJobSparepart =
                  "UPDATE job_sparepart SET orderedQuantity='$orderedQuantity' WHERE job_id ='" .
                  $jobId .
                  "' AND sparepartid='" .
                  $sparepart .
                  "'";
                $updateJobSparepart = mysqli_query($conn, $updateJobSparepart);
                if ($updateJobSparepart) {
                  echo 1;
                } else {
                  echo mysqli_error($conn);
                }
              }
            }
            $po_id = 0;
            // insert the purchase order when the sparepart quantity is less than stores
            if ($part["spQuantity"] < $part["orderedQuantity"]) {
              //check for an entry in the table purchase_order for the job
              $qrySelectPOid =
                "SELECT 	po_id FROM purchase_order WHERE job_id='" .
                $jobId .
                "'";
              $sqlSelectPOid = mysqli_query($conn, $qrySelectPOid);

              if ($sqlSelectPOid) {
                if (mysqli_num_rows($sqlSelectPOid) == 0) {
                  //   no pos found insert po
                  date_default_timezone_set("Asia/Calcutta");
                  $poTime = date("H:i");
                  $poDate = date("Y/m/d");
                  $insertPo = "INSERT INTO purchase_order (job_id,poTime,poDate) VALUES ('$jobId','$poTime','$poDate')";
                  $resultinsertPo = mysqli_query($conn, $insertPo);
                  if ($resultinsertPo) {
                    echo 1;
                  } else {
                    echo mysqli_error($conn);
                  }
                  //get po id from purchase_order
                  $checkSparepart =
                    "SELECT po_id FROM purchase_order WHERE job_id='" .
                    $jobId .
                    "'";
                  $sqlcheckSparepart = mysqli_query($conn, $checkSparepart);
                  if ($sqlcheckSparepart) {
                    if (mysqli_num_rows($sqlcheckSparepart) > 0) {
                      $row = mysqli_fetch_assoc($sqlcheckSparepart);
                      $po_id = $row["po_id"];
                    }
                  }
                  //get po id from purchase_order end
                  //check for the sparepart entry in the table po
                  $qryCheckPOid =
                    "SELECT * FROM po_sparepart WHERE spid ='" .
                    $sparepart .
                    "' AND po_id ='" .
                    $po_id .
                    "'";
                  $checkPoSpare = mysqli_query($conn, $sqlCheckPOid);
                  if ($checkPoSpare) {
                    //if no rows
                    if (mysqli_num_rows($checkPoSpare) == 0) {
                      $insertData = "INSERT INTO po_sparepart (po_id,spid) VALUES ('$po_id','$sparepart')";
                      $resultData = mysqli_query($conn, $insertData);
                      if ($resultData) {
                        echo 1;
                      } else {
                        echo mysqli_error($conn);
                      }
                    }
                    // else{
                    //row exists
                    // $updateJobSparepart = "UPDATE po_sparepart SET orderedQuantity='$orderedQuantity' WHERE po_id ='".$po_id."'";
                    // $updateJobSparepart = mysqli_query($conn, $updateJobSparepart);
                    // if ($updateJobSparepart) {
                    //   echo 1;
                    // } else {
                    //   echo mysqli_error($conn);
                    // } }
                  } else {
                    echo "Query failed " . $Query;
                  }
                } else {
                  // pos found
                  //get po id from purchase_order
                  $checkSparepart =
                    "SELECT po_id FROM purchase_order WHERE job_id='" .
                    $jobId .
                    "'";
                  $sqlcheckSparepart = mysqli_query($conn, $checkSparepart);
                  if ($sqlcheckSparepart) {
                    if (mysqli_num_rows($sqlcheckSparepart) > 0) {
                      $row = mysqli_fetch_assoc($sqlcheckSparepart);
                      $po_id = $row["po_id"];
                    }
                  }
                  //get po id from purchase_order end
                  //check for the sparepart entry in the table po
                  $qryCheckPart =
                    "SELECT * FROM po_sparepart WHERE spid ='" .
                    $sparepart .
                    "' AND po_id ='" .
                    $po_id .
                    "'";
                  $checkPoSparepart = mysqli_query($conn, $qryCheckPart);
                  if ($checkPoSparepart) {
                    //if no rows
                    if (mysqli_num_rows($checkPoSparepart) == 0) {
                      $insertData = "INSERT INTO po_sparepart (po_id,spid) VALUES ('$po_id','$sparepart')";
                      $resultData = mysqli_query($conn, $insertData);
                      if ($resultData) {
                        echo 1;
                      } else {
                        echo mysqli_error($conn);
                      }
                    }
                  } else {
                    echo "Query failed " . $Query;
                  }
                }
              }
            }
          }
        }
      }
    }
  }
}
//get all inspections
if (isset($_GET["inspectionlist"])) {
  $data["state"] = 1;
  $dataArr = [];
  $res = mysqli_query(
    $conn,
    "SELECT * FROM cliant c 
            INNER JOIN vehicle v ON c.cNic = v.ownerNic
            INNER JOIN job j ON v.vNo = j.vNo
            ORDER BY j.jobId DESC"
  );
  if (mysqli_num_rows($res) > 0) {
    while ($row = mysqli_fetch_assoc($res)) {
      array_push($dataArr, $row);
    }
    // output data of each row
    $data["data"] = $dataArr;
    $data["state"] = 1;
  } else {
    $data["state"] = 0;
  }
  header("Content-type: application/json; charset=utf-8"); //inform the browser we're sending JSON data
  echo json_encode($data); //echoing JSON encoded data as the response for the AJAX call
}
// get spareparts for the inspection
if (isset($_GET["sparepartlist"])) {
  $id = $_GET["id"];
  $data["state"] = 1;
  $dataArr = [];
  $res = mysqli_query(
    $conn,
    "SELECT * FROM job_sparepart js INNER JOIN spareparts s ON js.sparepartid = s.spId WHERE js.job_id=" .
      $id .
      ""
  );
  if (mysqli_num_rows($res) > 0) {
    while ($row = mysqli_fetch_assoc($res)) {
      array_push($dataArr, $row);
    }
    // output data of each row
    $data["data"] = $dataArr;
    $data["state"] = 1;
  } else {
    $data["state"] = 0;
  }
  header("Content-type: application/json; charset=utf-8"); //inform the browser we're sending JSON data
  echo json_encode($data); //echoing JSON encoded data as the response for the AJAX call
}
//delete spareparts in inspections
if (isset($_POST["act"])) {
  if ($_POST["act"] == "delete_sparepart") {
    $jId = $_POST["jid"];
    $sparepartid = $_POST["partid"];
    $delData =
      "DELETE FROM job_sparepart WHERE job_id='" .
      $jId .
      "'AND sparepartid='" .
      $sparepartid .
      "'";
    $resultData = mysqli_query($conn, $delData);
    if ($resultData) {
      echo 1;
    } else {
      echo mysqli_error($conn);
    }
  }
}
?>
